import java.util.*;
class Gravitacija {
    
    public static void izpis(double visina, double pospesek){
        System.out.printf("%f m%n%f m/s^2", visina, pospesek);
    }
    
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
     //   System.out.println("OIS je zakon!");
        System.out.printf("%f m/s^2%n",izracun(sc.nextDouble()));
    }
    
    static final double GRAVITACIJSKA_KONSTANTA = 6.674 * Math.pow(10, -11);
    static final double MASA_ZEMLJE = 5.972 * Math.pow(10, 24);
    static final double POLMER_ZEMLJE = 6.371 * Math.pow(10, 6);
    
    public static double izracun(double nadmorskaVisina) {
        return (GRAVITACIJSKA_KONSTANTA * MASA_ZEMLJE) / Math.pow(POLMER_ZEMLJE + nadmorskaVisina, 2);
    }
}